package com.witstec.advertisingscreen.act

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.witstec.advertisingscreen.R
import kotlinx.android.synthetic.main.activity_image_view.*

class ImageViewActivity : AppCompatActivity() {

    var path_file: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_view)
        path_file = intent.getStringExtra("path_url")
        Glide.with(this).load(path_file).into(cropImageView)

        btn_save.setOnClickListener {
            val ints = cropImageView.croppedImageInts
            EditActivity.x = ints[0]
            EditActivity.y = ints[1]
            EditActivity.w = ints[2]
            EditActivity.h = ints[3]
            finish()
        }

    }
}
