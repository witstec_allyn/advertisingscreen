package com.witstec.advertisingscreen;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

public class VideoOutView extends View {


    private static final String TAG = "FackMask";

    private Paint paint;
    private Rect rect;

    private int left;
    private int top;
    private int right;
    private int bottom;
    /*裁剪框遮罩层*/
    public Paint maskLayerPaint;
    public Path maskLayerPath;
    public Path cropPath;
    private int moveY;
    private Canvas canvas;
    private Bitmap savedBitmap;
    private PaintFlagsDrawFilter pfd;

    public VideoOutView(Context context) {
        super(context);
    }

    public VideoOutView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VideoOutView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPaint();
    }


    /**
     * 外部调用的接口
     */
    public void setRect(int left, int top, int right, int bottom, Bitmap savedBitmap) {
        this.left = left;
        this.top = top;
        this.savedBitmap = savedBitmap;
        this.right = right;
        this.bottom = bottom;
        Log.d(TAG, "setRect: 控件");
        invalidate();//更新调用onDraw重新绘制
    }


    private void initPaint() {
        paint = new Paint();
        left = 0;
        top = 400;
        right = 1080;
        bottom = 551;
        maskLayerPath = new Path();
        cropPath = new Path();
        maskLayerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        maskLayerPaint.setColor(Color.parseColor("#30000000"));
        maskLayerPaint.setStyle(Paint.Style.FILL);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        this.canvas = canvas;
        super.onDraw(canvas);
        canvas.setDrawFilter(pfd);
        canvas.drawBitmap(savedBitmap, 0, 0, new Paint());
        paint.setColor(Color.WHITE);
        paint.setStrokeWidth(10);
        paint.setStyle(Paint.Style.STROKE);//设置空心
        rect = new Rect(left, top, right, bottom);
        canvas.drawRect(rect, paint);
        //遮罩层
        if (!maskLayerPath.isEmpty()) {
            maskLayerPath.reset();
        }
        cropPath.addRect(new RectF(left, top, right, bottom), Path.Direction.CW);
        maskLayerPath.op(cropPath, Path.Op.XOR);
        maskLayerPath.addRect(new RectF(0, 0, 1080, 2340), Path.Direction.CW);
        canvas.drawPath(maskLayerPath, maskLayerPaint);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                moveY = y;
                break;
            case MotionEvent.ACTION_MOVE:
                //计算移动的距离
//                int offsetX = x - moveX;
                int offsetY = (y - moveY) / 8;
//                //调用layout方法来重新放置它的位置
//                offsetLeftAndRight(offsetX);
//                offsetTopAndBottom(offsetY);
                top = top + offsetY;
                bottom = bottom + offsetY;
                break;
        }
        invalidate();
        return true;
    }


}
